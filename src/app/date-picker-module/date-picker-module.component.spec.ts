import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerModuleComponent } from './date-picker-module.component';

describe('DatePickerModuleComponent', () => {
  let component: DatePickerModuleComponent;
  let fixture: ComponentFixture<DatePickerModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatePickerModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
